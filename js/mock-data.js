/**
 * JSon contenant les informations d'une session de formation ainsi que les stagiaires associés.
 */
let trainingSession = {
    "id": 1,
    "nom": "DWWM",
    "date": "10/30/2023",
    "start_hour": 13,
    "end_hour": 17,
    "stagiaires": [
        {
            "id": 1,
            "first_name": "Maxime",
            "last_name": "Gault",
            "date_inscription": "10/18/2021",
            "email": "cmyers0@ca.gov",
            "status": "NOT_SPECIFIED", // le status peut prendre 3 valeurs : "NOT_SPECIFIED", "PRESENT", "ABSENT"
            "has_signed": false
        },
        {
            "id": 2,
            "first_name": "Hugo",
            "last_name": "Fernandes",
            "date_inscription": "09/13/2021",
            "email": "rneeve1@va.gov",
            "status": "NOT_SPECIFIED",
            "has_signed": false
        },
        {
            "id": 3,
            "first_name": "Valentin",
            "last_name": "Gault",
            "date_inscription": "05/23/2023",
            "email": "ltookill2@irs.gov",
            "status": "NOT_SPECIFIED",
            "has_signed": false
        },
        {
            "id": 4,
            "first_name": "Jonathan",
            "last_name": "Bous",
            "date_inscription": "08/12/2021",
            "email": "tpamphilon3@istockphoto.com",
            "status": "NOT_SPECIFIED",
            "has_signed": false,
        },
        {
            "id": 5,
            "first_name": "Aurélie",
            "last_name": "Pires",
            "date_inscription": "09/16/2021",
            "email": "icollingridge4@webmd.com",
            "status": "NOT_SPECIFIED",
            "has_signed": false
        },
        {
            "id": 6,
            "first_name": "Loann",
            "last_name": "Bous",
            "date_inscription": "02/22/2022",
            "email": "ahan5@hhs.gov",
            "status": "NOT_SPECIFIED",
            "has_signed": false
        },
        {
            "id": 7,
            "first_name": "Séraphin",
            "last_name": "Invaincu",
            "date_inscription": "10/25/2023",
            "email": "nkearley6@noaa.gov",
            "status": "NOT_SPECIFIED",
            "has_signed": false
        },
        {
            "id": 8,
            "first_name": "Reda",
            "last_name": "Michoui",
            "date_inscription": "02/14/2022",
            "email": "crodenburg7@cocolog-nifty.com",
            "status": "NOT_SPECIFIED",
            "has_signed": false
        },
        {
            "id": 9,
            "first_name": "Cyril",
            "last_name": "Jariod",
            "date_inscription": "04/02/2023",
            "email": "rknighton8@artisteer.com",
            "status": "NOT_SPECIFIED",
            "has_signed": false
        },
        {
            "id": 10,
            "first_name": "Sergent",
            "last_name": "Gamos",
            "date_inscription": "04/14/2022",
            "email": "jbrisse9@comcast.net",
            "status": "NOT_SPECIFIED",
            "has_signed": false
        },
        {
            "id": 11,
            "first_name": "Todd",
            "last_name": "Bateman",
            "date_inscription": "11/08/2021",
            "email": "tbatemana@lycos.com",
            "status": "NOT_SPECIFIED",
            "has_signed": false
        },
        {
            "id": 12,
            "first_name": "Milli",
            "last_name": "Kingsford",
            "date_inscription": "04/05/2022",
            "email": "mkingsfordb@wordpress.org",
            "status": "NOT_SPECIFIED",
            "has_signed": false
        },
        {
            "id": 13,
            "first_name": "George",
            "last_name": "Vynarde",
            "date_inscription": "01/23/2023",
            "email": "gvynardec@china.com.cn",
            "status": "NOT_SPECIFIED",
            "has_signed": false
        },
        {
            "id": 14,
            "first_name": "Kelci",
            "last_name": "Rickerd",
            "date_inscription": "05/29/2023",
            "email": "krickerdd@last.fm",
            "status": "NOT_SPECIFIED",
            "has_signed": false
        },
        {
            "id": 15,
            "first_name": "Saxe",
            "last_name": "Larn",
            "date_inscription": "10/12/2022",
            "email": "slarne@360.cn",
            "status": "NOT_SPECIFIED",
            "has_signed": false
        }]

};
