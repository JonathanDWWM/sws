// récupération de la session à partir du fichier "mock-data.js"
let session = trainingSession;
let stagiaires = session.stagiaires;// récupération de tous les stagiaires

// Creation compteur signature
function updateSignatureCounter(stagiaires) { // Fonction de MAJ des compteurs de signature
    let signatureDiv = document.querySelector('.signe');
    let stagiaireSignature = stagiaires.filter(stagiaire => stagiaire.has_signed === true);
    if (signatureDiv) {
        signatureDiv.textContent = stagiaireSignature.length;
    }
}

// Creation compteur non renseigne
function updateNonRenseigneCounter(stagiaires) { // Fonction de MAJ du compteur des NON SPECIFIED
    let nonRenseigneDiv = document.querySelector('.nonrenseigne');
    let stagiaireNonRenseigne = stagiaires.filter(stagiaire => stagiaire.status === 'NOT_SPECIFIED'); // Filtre la liste pour garder que ceux avec le statut non spécifié
    if (nonRenseigneDiv) { // Verifier que la div nonrenseigne existe dans le doc HTML
        nonRenseigneDiv.textContent = stagiaireNonRenseigne.length; // Met à jour le texte avec le nombre d'élève
    }
}

// Creation compteur present
function updatePresentCounter(stagiaires) { // fonction de MAJ du compteur des présents
    let presenceDiv = document.querySelector('.presence');
    let stagiairePresent = stagiaires.filter(stagiaire => stagiaire.status === 'PRESENT');
    if (presenceDiv) { // Verifier que la div Presence existe dans le doc HTML
        presenceDiv.textContent = stagiairePresent.length;
    }
}

// Creation compteur absence
function updateAbsentCounter(stagiaires) {
    let absenceDiv = document.querySelector('.absence');
    let stagiaireAbsent = stagiaires.filter(stagiaire => stagiaire.status === 'ABSENT');
    if (absenceDiv) {
        absenceDiv.textContent = stagiaireAbsent.length;
    }
}

function creerEleveDiv(stagiaire) {
    let stagiaireDiv = document.createElement('div');// Créez une div pour chaque stagiaire
    stagiaireDiv.className = 'stagiaire';

    let infoStagiaire = document.createElement('div'); // Créer une div infostagiaire
    infoStagiaire.className = 'infostagiaire';

    let mailStagiaire = stagiaire.email;
    let nomStagiaire = stagiaire.first_name + ' ' + stagiaire.last_name;// Afficher le nom de chaque stagiaire
    infoStagiaire.innerHTML = `<p class="nomargin" >${nomStagiaire}</p> <p class="nomargin mail" >${mailStagiaire}</p>`;

    let presentDiv = document.createElement('div');
    presentDiv.className = 'present';

    let absentDiv = document.createElement('div');
    absentDiv.className = 'absent';

    let presentButton = document.createElement('img'); // Creation du boutton present
    presentButton.src = 'images/user-present.svg'; // je recupere l'image

    presentButton.addEventListener('click', () => { // Ajoute un évènement au clique de la souris
        if (stagiaire.status !== 'PRESENT') {
            stagiaire.status = 'PRESENT';
            stagiaireDiv.style.backgroundColor = 'lightgreen';

            updateNonRenseigneCounter(stagiaires); // Met à jour le compteur non renseigné 
            updatePresentCounter(stagiaires); // Met à joour le compteur present
            updateAbsentCounter(stagiaires);

            presentDiv.style.visibility = 'hidden'; // Cache le bouton "Présent"
            absentDiv.style.visibility = 'visible'; // Remet le bouton absent
        }
    });

    let absentButton = document.createElement('img'); // Creation du boutton absent
    absentButton.src = 'images/user-absent.svg'; // je recupere l'image

    absentButton.addEventListener('click', () => { // Quand le bouton absent est cliqué 
        if (stagiaire.status !== 'ABSENT') { // Si l'élève n'est pas absent,  change son statut en absent et MAJ le compteur
            stagiaire.status = 'ABSENT';
            stagiaireDiv.style.backgroundColor = 'lightcoral';
            updateNonRenseigneCounter(stagiaires); // MAJ Du compteur (appel de la fonction)
            updateAbsentCounter(stagiaires); // Maj du compteur absent
            updatePresentCounter(stagiaires);

            absentDiv.style.visibility = 'hidden'; // Cache le bouton "absent"
            presentDiv.style.visibility = 'visible'; // Remet le bouton present
        } else if (stagiaire.status === 'ABSENT') { // Si le stagiaire est deja absent, change en present et MAJ le compteur
            stagiaire.status = 'PRESENT';
            stagiaireDiv.style.backgroundColor = 'lightgreen';
            updateNonRenseigneCounter(stagiaires);
            updatePresentCounter(stagiaires);
            updateAbsentCounter(stagiaires);

            presentDiv.style.visibility = 'hidden';
            absentDiv.style.visibility = 'visible';
        }
    });

    presentDiv.appendChild(presentButton);
    absentDiv.appendChild(absentButton);
    stagiaireDiv.appendChild(presentDiv);
    stagiaireDiv.appendChild(absentDiv);
    stagiaireDiv.appendChild(infoStagiaire);

    if (stagiaire.status === 'NOT_SPECIFIED') { // Ajoutez cette div à la section correspondante
        stagiaireDiv.style.backgroundColor = 'lightgray'; // Fond gris clair pour les non spécifiés
    }
    document.getElementById('stagiaires-section').appendChild(stagiaireDiv);
}

function TrierEtudiant(stagiaires) { // Fonction pour trier la liste des etudiants 
    stagiaires.sort((a, b) => { // methode sort pour trier la liste
        const nomA = (a.last_name).toLowerCase(); // Creation de chaine en minuscule avec nom complet
        const nomB = (b.last_name).toLowerCase();
        if (nomA < nomB) return -1; // Comparaison des noms en minuscule pour determiner l'ordre
        if (nomA > nomB) return 1;
        return 0;
    })
}

document.addEventListener("DOMContentLoaded", () => {

    let nombreStagiaire = document.querySelector('.nombre');
    nombreStagiaire.textContent = stagiaires.length;

    const ajoutEleveBouton = document.getElementById('ajoutelevebouton');
    const ajoutEleveForm = document.getElementById('ajout-eleve-form');

    if (ajoutEleveBouton && ajoutEleveForm) {
        ajoutEleveBouton.addEventListener('click', () => {
            ajoutEleveForm.style.display = 'flex';
        });
        ajoutEleveForm.addEventListener('submit', (event) => {
            event.preventDefault(); // Empêche la soumission par defaut du formulaire
            const formData = new FormData(ajoutEleveForm);
            const nouvelEleve = {
                first_name: formData.get('first_name'),
                last_name: formData.get('last_name'),
                email: formData.get('email'),
                status: formData.get('status')

            };
            stagiaires.push(nouvelEleve) // Ajoute le nouvel élève

            updateSignatureCounter(stagiaires); // Appel fonction MAJ compteur signé
            updatePresentCounter(stagiaires); // Appel fonction MAJ compteur present
            updateAbsentCounter(stagiaires); // Appel fonction MAJ compteur absent
            updateNonRenseigneCounter(stagiaires); // Appel fonction MAJ compteur non renseigné
            creerEleveDiv(nouvelEleve); // Ajout de toutes les divs
            nombreStagiaire.textContent = stagiaires.length;
            ajoutEleveForm.style.display = 'none';
        });
    }
    
    updateSignatureCounter(stagiaires);
    updateNonRenseigneCounter(stagiaires); // Appeler la fonction de MAJ du compteur pour l'executer 
    updatePresentCounter(stagiaires);
    updateAbsentCounter(stagiaires);

    for (let i = 0; i < stagiaires.length; i++) {// boucle sur tous les stagaires
        let stagiaire = stagiaires[i];//  console.log(stagiaires[i]);
        creerEleveDiv(stagiaire);

        document.getElementById("triBouton").addEventListener("click", () => {
            TrierEtudiant(stagiaires); // Trier les etudiants
            const stagiairesSection = document.getElementById("stagiaires-section");
            stagiairesSection.innerHTML = ""; // Efface le contenu actuel 
            stagiaires.forEach(function (etudiant) {
                creerEleveDiv(etudiant);
            });
        });
    };
});
